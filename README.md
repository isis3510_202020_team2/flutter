# Brigapp 🚨:

Brigapp is an app that helps BEU (Brigada estudiantil uniandina) to manage emergencies. 

## Features:
- Report emergencies
- Login/register as a brigadista or as a generic user
- Manage emergencies, ask for backup and close emergencies
- Automatically turn on flashligt in low light conditions

## How to deploy it locally:

For deploying the app in Android:
- clone the repo
- open the repo directory in Android Studio
- run `flutter pub get ` in the terminal
- connect your device and select the `lib/main.dart` file (you can also create a new device in the avd manager and run it with that device. If you don't see the avd device in the selected devices, cold boot the device then run the app )
- click the run button

## Demo:

[![Login/Signup connectivity demo](http://img.youtube.com/vi/air_DPCw6js/0.jpg)](http://www.youtube.com/watch?v=air_DPCw6js "Login/Signup connectivity demo")

[![Report demo](https://i9.ytimg.com/vi/Ca7722dfUqA/mq2.jpg?sqp=CNy-9PwF&rs=AOn4CLCvMjdIa538YQNEIGekX2uq2S3Jrg)](https://youtu.be/Ca7722dfUqA "Report Emergency demo")

